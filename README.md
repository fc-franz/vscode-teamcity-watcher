# teamcity-build-status-watcher 

This is a small extension this can be used to observe a TeamCity track and color the status bar depending on the build status. 

IT IS FOR AWAY FROM BEING READY TO USE !!!


| Success | Fail |
| ------ | ------ |
| \!\[Success\]\(assets/success.png\) | \!\[Fail\]\(assets/fail.png\) |


**Still missing / future work:** 
* reset statusbar on editor close
* fix some bugs
* add documentation
* write tests
* show extra notification if own changes could have broke the build
