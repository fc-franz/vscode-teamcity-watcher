// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { Watcher } from "./watcher";


let watcher: Watcher;
const MINUTE_IN_MILLISECONDS = 60_000;


// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(): void {
	start();
}


function start() {
	const extensionConfig = vscode.workspace.getConfiguration("teamcity-watcher");

	const serverUrl = extensionConfig.get("serverUrl") as string;
	if (!serverUrl) {
		vscode.window.showErrorMessage("Server URL setting [serverUrl] not provided!");
		return;
	}

	const authenticationToken = extensionConfig.get("authenticationToken") as string;
	if (!authenticationToken) {
		vscode.window.showErrorMessage("Authentication token setting [authenticationToken] not provided!");
		return;
	}


	const updateInterval = extensionConfig.get("updateInterval") as number;
	if (!updateInterval) {
		vscode.window.showErrorMessage("Update interval setting [updateInterval] not provided!");
		return;
	}


	const trackId = extensionConfig.get("trackId") as string;
	if (!trackId) {
		vscode.window.showErrorMessage("Track id setting [trackId] not provided!");
		return;
	}

	if (watcher) {
		watcher.stop();
	}

	watcher = new Watcher(
		serverUrl,
		authenticationToken,
		updateInterval * MINUTE_IN_MILLISECONDS,
		trackId
	);
	watcher.start();
}


// this method is called when your extension is deactivated
export function deactivate(): void {
	if (watcher) {
		watcher.stop();
	}
}

