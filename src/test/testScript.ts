import { BuildApi, ChangeApi, OAuth } from "../generated/api";


const serverUrl = process.argv[process.argv.length - 3];
const token = process.argv[process.argv.length - 2];
const trackId = process.argv[process.argv.length - 1];

console.log(`ServerUrl: ${serverUrl}`);
console.log(`Token: ${token}`); 
console.log(`Track id: ${trackId}`);

(async function () {

  const buildApi = new BuildApi(serverUrl);
  const changeApi = new ChangeApi(serverUrl);
  const auth = new OAuth();
  auth.accessToken = token;

  buildApi.setDefaultAuthentication(auth);
  changeApi.setDefaultAuthentication(auth);

  /*   try {
      console.log(await api.serveAggregatedBuildStatus("buildType(id:bt2,count:1)"));
    } catch (e) {
      console.error(e);
    } */


  /*  */

  try {
    console.log(await buildApi.serveBuild(`buildType(id:${trackId}),count:1,running:true,personal:false`));
  } catch (e) {
    console.error(e);
  }

})();